/**
 *
 * @param container
 * @param slideWidth
 * @param auto
 * @constructor build()
 */
function Carousel(container, slideWidth, auto) {

    /**
     * Contexte
     * @type {Carousel}
     */
    var ctx = this;

    /**
     * Conteneur
     * @type {HTMLElement}
     */
    this.container = container;

    /**
     *
     * @type {NodeList}
     */
    this.slideStack = container.getElementsByClassName('carousel-slide');

    /**
     *
     * @type {NodeList|*}
     */
    this.dotStack = document.getElementById('carousel-map').getElementsByClassName('carousel-dot');

    /**
     *
     * @type {number}
     */
    this.nbSlides = this.slideStack.length;

    /**
     *
     * @type {number}
     */
    this.index = 0;

    /**
     *
     * @type {number}
     */
    this.translate = 0;

    /**
     *
     * @type {*|boolean}
     */
    this.auto = auto || false;

    /**
     * Largeur d'une slide
     * @type {number}
     */
    this.slideWidth = slideWidth;

    /**
     *
     * @type {number}
     */
    this.translateIndex = 100 / this.nbSlides;

    /**
     * Slide précédente
     */
    this.prevSlide = function () {
        this.index--;
        if (this.index < 0) {
            this.index = this.nbSlides - 1;
        }
        this.changeSlide(this.index);
    };

    /**
     * Slide suivante
     */
    this.nextSlide = function () {
        this.index++;
        if (this.index > (this.nbSlides - 1)) {
            this.index = 0;
        }
        this.changeSlide(this.index);
    };

    /**
     * Setup le carousel
     */
    this.build = function () {
        var contWidth = this.slideWidth * this.nbSlides;
        container.style.width = contWidth + "px";
        document.getElementById('carousel-btn-left').addEventListener("click",function() {
           ctx.prevSlide();
        });
        document.getElementById('carousel-btn-right').addEventListener("click",function() {
           ctx.nextSlide();
        });
        for (var i = 0; i < this.nbSlides; i++) {
            this.callback(i);
        }
        this.updateMap();
        if (this.auto == true) {
            this.autoChange();
        }
    };

    /**
     * Callback pour build()
     * @param i
     */
    this.callback = function(i){
        this.dotStack.item(i).addEventListener("click", function () {
            carousel.changeSlide(i);
        })
    };

    /**
     * Change la slide pour celle spécifiée
     * @param {number} index
     */
    this.changeSlide = function (index) {
        this.translate = -(this.translateIndex * index);
        this.index = index;
        container.style.transform = "translateX(" + this.translate + "%)";
        this.updateMap();
    };

    /**
     * Update les points de nav du carousel
     */
    this.updateMap = function () {
        for (var i = 0; i < this.nbSlides; i++) {
            this.dotStack.item(i).classList.remove('active');
        }
        this.dotStack.item(this.index).classList.add('active');
    };

    /**
     * Carousel automatique
     */
    this.autoChange = function () {
        if (this.auto == true) {
            setTimeout(function () {
                ctx.nextSlide()
            }, 10000);

            setTimeout(function () {
                ctx.autoChange()
            }, 10000);
        }
    }

}